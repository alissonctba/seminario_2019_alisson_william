package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class AreaCientifica {

    private Long id;
    private String nome;
    private List<Curso> listaCurso = new ArrayList<>();

    public void adicionarCurso(Curso curso) {
        this.listaCurso.add(curso);
    }

    public boolean possuiCurso(Curso curso) {
        if (this.listaCurso.isEmpty()) {
            return false;
        } else {
            return this.listaCurso.contains(curso);
        }
    }

    public List<Curso> getCursos() {
        return this.listaCurso;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
