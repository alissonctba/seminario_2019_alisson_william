package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class Seminario {

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private java.util.Date data;
    private Integer qtdInscricoes;

    private List<Professor> listaProfessor = new ArrayList<>();
    private List<AreaCientifica> listaAreaCientifica = new ArrayList<>();
    private List<Inscricao> listaInscricao = new ArrayList<>();

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
        for (int i = 0; i < qtdInscricoes; i++) {
            Inscricao novaInscricao = new Inscricao(this);
            this.adicionarInscricao(novaInscricao);
        }
        this.adicionarAreaCientifica(areaCientifica);

        this.adicionarProfessor(professor);
        professor.adicionarSeminario(this);
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.listaInscricao.add(inscricao);
    }

    public void adicionarAreaCientifica(AreaCientifica areaCientifica) {
        this.listaAreaCientifica.add(areaCientifica);
    }

    public void adicionarProfessor(Professor professor) {
        this.listaProfessor.add(professor);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public java.util.Date getData() {
        return this.data;
    }

    public void setData(java.util.Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public List<Professor> getProfessores() {
        return this.listaProfessor;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.listaAreaCientifica;
    }

    public List<Inscricao> getInscricoes() {
        return this.listaInscricao;
    }
}
